package payment

import (
	"net/http"

	"gitlab.com/edsonmichaque/px/internal"
)

func New() *Handler {
	return &Handler{
		svc: nil,
	}
}

type Handler struct {
	internal.Handler
	svc PaymentService
}

func (h Handler) GetPayment(w http.ResponseWriter, r *http.Request) {

}
