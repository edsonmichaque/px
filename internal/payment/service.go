package payment

type PaymentService interface {
	CreatePayment(*Payment) (*Payment, error)
}
