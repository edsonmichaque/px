package internal

type Logger interface {
	Error(args ...interface{})
	Errorf(args ...interface{})
	Warn(args ...interface{})
	Warnf(args ...interface{})
	Debug(args ...interface{})
	Debugf(args ...interface{})
	Info(args ...interface{})
	Infof(args ...interface{})
}
