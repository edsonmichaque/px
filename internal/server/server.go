package server

import "net/http"

type Server struct {
	handler http.Handler
}

func New() *Server {
	return &Server{
		handler: router(),
	}
}
