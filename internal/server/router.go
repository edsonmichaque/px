package server

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/edsonmichaque/px/internal/payment"
	"gitlab.com/edsonmichaque/px/internal/transfer"
)

func router() http.Handler {
	r := mux.NewRouter()

	v1 := r.PathPrefix("/v1").Subrouter()

	v1.HandleFunc("/payments", payment.New().GetPayment).Methods(http.MethodGet)
	v1.HandleFunc("/transfers", transfer.New().GetTransfer).Methods(http.MethodGet)

	return r
}
