package transfer

type TransferService interface {
	CreatePayment(*Payment) (*Payment, error)
}
