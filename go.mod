module gitlab.com/edsonmichaque/px

go 1.17

replace gitlab.com/edsonmichaque/px => ./

require github.com/gorilla/mux v1.8.0 // indirect
